# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl test.pl'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test;
BEGIN { plan tests => 16 };
use Video::ivtv;
my $ivtvObj = Video::ivtv->new();
ok (defined $ivtvObj);

my $dev = "/dev/video0";
open (my $tuner, "<$dev") or die "Error opening $dev!  $!\n";
my $tunerFD = fileno($tuner);
my ($oldwidth, $oldheight) = $ivtvObj->getResolution($tunerFD);
print "Width = $oldwidth, Height = $oldheight\n";
ok ($oldwidth != -1 && $oldheight != -1);

my $result = $ivtvObj->setResolution($tunerFD, 640, 480);
if (defined $result)
{
	if ($result)
	{
		my ($width, $height) = $ivtvObj->getResolution($tunerFD);
		if ($width == 640 && $height == 480)
		{
			ok(3);
      # restore the previous setting.
      $result = $ivtvObj->setResolution($tunerFD, $oldwidth, $oldheight);
		}
		else
		{
			print "Error:  width='$width', height='$height'\n";
      ok ($width ==640 && $height == 480);
		}
	}
	else
	{
		ok ($result);
	}
}
else
{
  ok (defined $result);
}

my $standard = $ivtvObj->getStandard($tunerFD);
ok ($standard == 0x00003000 || $standard == 0x000000FF || $standard == 0x007F0000);

# set the Video Standard to NTSC (0x3000)
$result = $ivtvObj->setStandard($tunerFD, 0x00003000);
if ($result)
{
  my $newStandard = $ivtvObj->getStandard($tunerFD);
  ok ($newStandard == $standard);
  # restore the previous standard.
  $ivtvObj->setStandard($tunerFD, $standard);
}
else
{
	ok ($result);
}

my $freq = $ivtvObj->getFrequency($tunerFD, 0);
ok ($freq != 0);

# set the Video Frequency to 8276 (SCiFI)
$result = $ivtvObj->setFrequency($tunerFD, 0, 8276);
if ($result)
{
  # verify the frequency got set to what we want.
  my $newfreq = $ivtvObj->getFrequency($tunerFD, 0);
  ok ($newfreq == 8276);
  # restore the previous frequency value
  $result = $ivtvObj->setFrequency($tunerFD, 0, $freq);
}
else
{
	ok ($result);
}

# get the Video Input
my $input = $ivtvObj->getInput($tunerFD);
ok ($input >= 0);
print "input = '$input'\n";

# set the Video Input
$result = $ivtvObj->setInput($tunerFD, 4); # Tuner on my card.
if ($result)
{
  # verify the input got set to what I wanted.
  my $newInput = $ivtvObj->getInput($tunerFD);
  ok ($newInput, 4, "Input = '$newInput' instead of 4!");
  # reset the input back to what it was originally.
  $ivtvObj->setInput($tunerFD, $input);
}
else
{
  ok ($result);
}

# enumerate the available Standards
my $done = 0;
my $index;
for ($index=0; !$done; ++$index)
{
  my ($i, $id, $name, $numerator, $denominator, $framelines) = $ivtvObj->enumerateStandard($tunerFD, $index);
  if ($i == -1)
  {
    $done = 1;  # this index is invalid.
  }
  else
  {
    printf("%d 0x%08x %s %d/%d %d\n", $i, $id, $name, $numerator, $denominator, $framelines);
  }
}
ok ($index > 0);

# enumerate the available Inputs
$done = 0;
$index;
for ($index=0; !$done; ++$index)
{
  my ($i, $name, $type, $audioset, $tuner, $std, $status) = $ivtvObj->enumerateInput($tunerFD, $index);
  if ($i == -1)
  {
    $done = 1;  # this index is invalid.
  }
  else
  {
    printf("%d %s 0x%08x 0x%08x 0x%08x 0x%08x %d\n", $i, $name, $type, $audioset, $tuner, $std, $status);
  }
}
ok ($index > 0);

# get the capabilities and make sure we are working with the ivtv driver before doing the next tests.
my @capabilities = $ivtvObj->getCapabilities($tunerFD);
if (scalar @capabilities != scalar(keys %{$ivtvObj->{capIndexes}}))
{
  ok (12, 0, "getCapabilities() returned " . int(@capabilities) . " values!\n");
}
else
{
  if ($capabilities[$ivtvObj->{capIndexes}{driver}] ne "ivtv")
  {
    print "I cannot test the ivtv codec options as your v4l2 driver = '$capabilities[$ivtvObj->{capIndexes}{driver}]' instead of 'ivtv'.\n";
    ok(12);
  }
  else
  {
    print "driver = $capabilities[$ivtvObj->{capIndexes}{driver}]\n";
    print "card = $capabilities[$ivtvObj->{capIndexes}{card}]\n";
    print "bus_info = $capabilities[$ivtvObj->{capIndexes}{bus_info}]\n";
    print "version = $capabilities[$ivtvObj->{capIndexes}{version}]\n";
    print "capabilities = $capabilities[$ivtvObj->{capIndexes}{capabilities}]\n";
    ok(12);

    # get the current codec settings
    my @codec = $ivtvObj->getCodecInfo($tunerFD);
    if (scalar @codec == scalar(keys %{$ivtvObj->{codecIndexes}}))
    {
      print "Current codec info:\n";
      print "aspect: $codec[$ivtvObj->{codecIndexes}{aspect}]\naudio_bitmask: $codec[$ivtvObj->{codecIndexes}{audio_bitmask}]\nbframes: $codec[$ivtvObj->{codecIndexes}{bframes}]\n";
      print "bitrate_mode: $codec[$ivtvObj->{codecIndexes}{bitrate_mode}]\nbitrate: $codec[$ivtvObj->{codecIndexes}{bitrate}]\nbitrate_peak: $codec[$ivtvObj->{codecIndexes}{bitrate_peak}]\ndnr_mode: $codec[$ivtvObj->{codecIndexes}{dnr_mode}]\n";
      print "dnr_spatial: $codec[$ivtvObj->{codecIndexes}{dnr_spatial}]\ndnr_temporal: $codec[$ivtvObj->{codecIndexes}{dnr_temporal}]\ndnr_type: $codec[$ivtvObj->{codecIndexes}{dnr_type}]\n";
      print "framerate: $codec[$ivtvObj->{codecIndexes}{framerate}]\nframespergop: $codec[$ivtvObj->{codecIndexes}{framespergop}]\ngop_closure: $codec[$ivtvObj->{codecIndexes}{gop_closure}]\n";
      print "pulldown: $codec[$ivtvObj->{codecIndexes}{pulldown}]\nstream_type: $codec[$ivtvObj->{codecIndexes}{stream_type}]\n";
      ok(13);
    }
    else
    {
      ok (13, 0, "getCodecInfo() returned " . int(@codec) . " values!\n");
    }

    # test setting them, then restore
    my @newCodec = $ivtvObj->getCodecInfo($tunerFD);
    $newCodec[$codecIndexes{bitrate}] = 6500000; # change the bitrate value
    $newCodec[$codecIndexes{bitrate_peak}] = 9000000; # change the bitrate_peak value
    $result = $ivtvObj->setCodecInfo($tunerFD, @newCodec);
    if ($result)
    {
      # verify that the values are what I set.
      my @testCodec = $ivtvObj->getCodecInfo($tunerFD);
      if ($testCodec[$ivtvObj->{codecIndexes}{bitrate}] == $newCodec[$ivtvObj->{codecIndexes}{bitrate}] && $testCodec[$ivtvObj->{codecIndexes}{bitrate_peak}] == $newCodec[$ivtvObj->{codecIndexes}{bitrate_peak}])
      {
        # restore the original values.
        $result = $ivtvObj->setCodecInfo($tunerFD, @codec);
        ok(14);
      }
      else
      {
        ok (14, 0, "getCodecInfo() returned bitrate = '$testCodec[$ivtvObj->{codecIndexes}{bitrate}]', bitrate_peak = '$testCodec[$ivtvObj->{codecIndexes}{bitrate_peak}]'\n");
      }
    }
    else
    {
      ok (0);
    }

    # verify the setEndGOP method is correctly bombing out if given invalid parameters.
    $result = $ivtvObj->setEndGOP($tunerFD, -1);
    ok (undef, $result, "setEndGOP(-1) returned '$result'\n");

    $result = $ivtvObj->setEndGOP($tunerFD, 2);
    ok (undef, $result, "setEndGOP(2) returned '$result'\n");
  }
}

#########################

# Insert your test code below, the Test module is use()ed here so read
# its man page ( perldoc Test ) for help writing this test script.

