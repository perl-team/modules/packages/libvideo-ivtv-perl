#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include <linux/types.h>

/* we don't want linux/time.h */
#define _LINUX_TIME_H 1
#include "videodev2.h"
#include <stdio.h>
#include "ivtv.h"


MODULE = Video::ivtv		PACKAGE = Video::ivtv		

void
getResolution(SV * self, IN int fd, OUTLIST int width, OUTLIST int height)
	INIT:
		struct v4l2_format vfmt;

	CODE:
	  vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if (ioctl(fd, VIDIOC_G_FMT, &vfmt) < 0)
		{
			/* return an invalid case to signal an error occured */
			width = height = -1;
		}
		else
		{
			width = vfmt.fmt.pix.width;
			height = vfmt.fmt.pix.height;
		}

int
setResolution(SV * self, int fd, int width, int height)
	INIT:
		struct v4l2_format vfmt;

    /* make sure we have valid input values. */
    if (width < 0 || width > 720)
    {
			XSRETURN_UNDEF;
		}
		if (height < 0 || height > 576)
		{
			XSRETURN_UNDEF;
		}
	CODE:
		vfmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;  /* make sure the buf type is set for Video Capture! */
    vfmt.fmt.pix.width = width;
		vfmt.fmt.pix.height = height;
		if (ioctl(fd, VIDIOC_S_FMT, &vfmt) < 0)
		{
			/* return an invalid case to signal an error occured */
			RETVAL = 0;
		}
    else
    {
			RETVAL = 1;
		}
	OUTPUT:
		RETVAL


int
getStandard(SV * self, int fd)
  INIT:
    v4l2_std_id n;
  CODE:
    if (ioctl(fd, VIDIOC_G_STD, &n) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = n;
    }
  OUTPUT:
    RETVAL

int
setStandard(SV * self, int fd, int standard)
  CODE:
    if (ioctl(fd, VIDIOC_S_STD, &standard) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = 1;
    }
  OUTPUT:
    RETVAL

void
enumerateStandard(SV * self, IN int fd, IN_OUTLIST int index, OUTLIST int std_id, OUTLIST char *name, OUTLIST int numerator, OUTLIST int denominator, OUTLIST int framelines)
	INIT:
		struct v4l2_standard vs;

    if (index < 0)
    {
			XSRETURN_UNDEF;
		}
	CODE:
    vs.index = index;
		if (ioctl(fd, VIDIOC_ENUMSTD, &vs) < 0)
		{
			/* return an invalid case to signal an error occured */
			index = -1;
      name = "";
      numerator = 0;
      denominator = 0;
      framelines = 0;
		}
		else
		{
      std_id = vs.id;
      name = vs.name;
      numerator = vs.frameperiod.numerator;
      denominator = vs.frameperiod.denominator;
      framelines = vs.framelines;
		}

int
getFrequency(SV * self, int fd, int tuner)
  INIT:
    struct v4l2_frequency vf;

    if (tuner < 0)
    {
			XSRETURN_UNDEF;
		}
  CODE:
    vf.tuner = tuner;
    if (ioctl(fd, VIDIOC_G_FREQUENCY, &vf) < 0)
    {
      RETVAL = -1;
    }
    else
    {
      RETVAL = vf.frequency;
    }
  OUTPUT:
    RETVAL

int
setFrequency(SV * self, int fd, int tuner, int freq)
  INIT:
    struct v4l2_frequency vf;

    if (tuner < 0)
    {
			XSRETURN_UNDEF;
		}
    if (freq < 0)
    {
			XSRETURN_UNDEF;
    }
  CODE:
    vf.tuner = tuner;
    vf.frequency = freq;
    if (ioctl(fd, VIDIOC_S_FREQUENCY, &vf) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = 1;
    }
  OUTPUT:
    RETVAL

int
getInput(SV * self, int fd)
  INIT:
    int input;
  CODE:
    if (ioctl(fd, VIDIOC_G_INPUT, &input) < 0)
    {
      RETVAL = -1;
    }
    else
    {
      RETVAL = input;
    }
  OUTPUT:
    RETVAL

int
setInput(SV * self, int fd, int input)
  INIT:
    if (input < 0)
    {
			XSRETURN_UNDEF;
		}
  CODE:
    if (ioctl(fd, VIDIOC_S_INPUT, &input) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = 1;
    }
  OUTPUT:
    RETVAL

void
enumerateInput(SV * self, IN int fd, IN_OUTLIST int index, OUTLIST char *name, OUTLIST int type, OUTLIST int audioset, OUTLIST int tuner, OUTLIST int std, OUTLIST int status)
	INIT:
		struct v4l2_input vi;

    if (index < 0)
    {
			XSRETURN_UNDEF;
		}
	CODE:
    vi.index = index;
		if (ioctl(fd, VIDIOC_ENUMINPUT, &vi) < 0)
		{
			/* return an invalid case to signal an error occured */
			index = -1;
      name = "";
      type = 0;
      audioset = 0;
      tuner = 0;
      std = 0;
      status = 0;
		}
		else
		{
      name = vi.name;
      type = vi.type;
      audioset = vi.audioset;
      tuner = vi.tuner;
      std = vi.std;
      status = vi.status;
		}

void
getCodecInfo(SV * self, IN int fd, OUTLIST int aspect, OUTLIST int audio_bitmask, OUTLIST int bframes, OUTLIST int bitrate_mode, OUTLIST int bitrate, OUTLIST int bitrate_peak, OUTLIST int dnr_mode, OUTLIST int dnr_spatial, OUTLIST int dnr_temporal, OUTLIST int dnr_type, OUTLIST int framerate, OUTLIST int framespergop, OUTLIST int gop_closure, OUTLIST int pulldown, OUTLIST int stream_type)
	INIT:
		struct ivtv_ioctl_codec codec;

	CODE:
		if (ioctl(fd, IVTV_IOC_G_CODEC, &codec) < 0)
		{
      /* an error occured, hopefully this is returning an empty array. */
		}
		else
		{
      aspect = codec.aspect;
      audio_bitmask = codec.audio_bitmask;
      bframes = codec.bframes;
      bitrate_mode = codec.bitrate_mode;
      bitrate = codec.bitrate;
      bitrate_peak = codec.bitrate_peak;
      dnr_mode = codec.dnr_mode;
      dnr_spatial = codec.dnr_spatial;
      dnr_temporal = codec.dnr_temporal;
      dnr_type = codec.dnr_type;
      framerate = codec.framerate;
      framespergop = codec.framespergop;
      gop_closure = codec.gop_closure;
      pulldown = codec.pulldown;
      stream_type = codec.stream_type;
		}

int
setCodecInfo(SV * self, int fd, int aspect, int audio_bitmask, int bframes, int bitrate_mode, int bitrate, int bitrate_peak, int dnr_mode, int dnr_spatial, int dnr_temporal, int dnr_type, int framerate, int framespergop, int gop_closure, int pulldown, int stream_type)
	INIT:
		struct ivtv_ioctl_codec codec;

    /* make sure we have valid input values. */

	CODE:
    codec.aspect = aspect;
    codec.audio_bitmask = audio_bitmask;
    codec.bframes = bframes;
    codec.bitrate_mode = bitrate_mode;
    codec.bitrate = bitrate;
    codec.bitrate_peak = bitrate_peak;
    codec.dnr_mode = dnr_mode;
    codec.dnr_spatial = dnr_spatial;
    codec.dnr_temporal = dnr_temporal;
    codec.dnr_type = dnr_type;
    codec.framerate = framerate;
    codec.framespergop = framespergop;
    codec.gop_closure = gop_closure;
    codec.pulldown = pulldown;
    codec.stream_type = stream_type;
		if (ioctl(fd, IVTV_IOC_S_CODEC, &codec) < 0)
		{
			/* return an invalid case to signal an error occured */
			RETVAL = 0;
		}
    else
    {
			RETVAL = 1;
		}
	OUTPUT:
		RETVAL

void
getCapabilities(SV * self, IN int fd, OUTLIST char *driver, OUTLIST char *card, OUTLIST char *bus_info, OUTLIST int version, OUTLIST int capabilities)
	INIT:
		struct v4l2_capability vcap;

	CODE:
		if (ioctl(fd, VIDIOC_QUERYCAP, &vcap) < 0)
		{
      /* an error occured, hopefully this is returning an empty array. */
		}
		else
		{
      driver = vcap.driver;
      card = vcap.card;
      bus_info = vcap.bus_info;
      version = vcap.version;
      capabilities = vcap.capabilities;
		}

int
setEndGOP(SV * self, int fd, int end_gop)
  INIT:
    if (end_gop < 0 || end_gop > 1)
    {
			XSRETURN_UNDEF;
		}
  CODE:
    if (ioctl(fd, IVTV_IOC_S_GOP_END, &end_gop) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = 1;
    }
  OUTPUT:
    RETVAL

int
stopEncoding(SV * self, int fd)
  CODE:
    if (ioctl(fd, VIDIOC_STREAMOFF) < 0)
    {
      RETVAL = 0;
    }
    else
    {
      RETVAL = 1;
    }
  OUTPUT:
    RETVAL

