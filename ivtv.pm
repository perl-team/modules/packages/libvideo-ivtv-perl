package Video::ivtv;

use 5.006;
use strict;
use warnings;

require Exporter;
require DynaLoader;

use vars qw($VERSION @ISA @EXPORT);

@ISA = qw(Exporter DynaLoader);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

@EXPORT = qw(
);

$VERSION = '0.13';

bootstrap Video::ivtv $VERSION;

sub new
{
  my $that = shift;
  my $class = ref($that) || $that;
  my $self = bless {}, $class;

  $self->{codecIndexes} = {
    aspect => 0,
    audio_bitmask => 1,  # audio renamed to audio_bitmask
    bframes => 2,
    bitrate_mode => 3,
    bitrate => 4,
    bitrate_peak => 5,
    dnr_mode => 6,
    dnr_spatial => 7,
    dnr_temporal => 8,
    dnr_type => 9,
    framerate => 10,
    framespergop => 11,
    gop_closure => 12,
    pulldown => 13,
    stream_type => 14,
  };

  $self->{capIndexes} = {
    driver => 0,
    card => 1,
    bus_info => 2,
    version => 3,
    capabilities => 4,
  };
  
  return $self;
}

# Preloaded methods go here.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Video::ivtv - Perl extension for using V4l2 in the ivtv perl scripts

=head1 SYNOPSIS

  use Video::ivtv;

  open(my $tuner, "</dev/video0") or die "Error opening /dev/video0! $!";

  my $tunerFD = fileno($tuner);  # the C functions need the file handle #.
  
  # initalize the Video::ivtv module
  my $ivtvObj = Video::ivtv->new();

  # get the current resolution
  my ($width, $height) = $ivtvObj->getResolution($tunerFD);

  # set the new resolution
  $ivtvObj->setResolution($tunerFD, 640, 480);

  close($tuner);

=head1 DESCRIPTION

The Video::ivtv module will provide helper methods for working with
videodev2.h structures and making ioctl calls that have proven to be
too difficult to create pack strings for in perl itself.

This is not supposed to be an equivalent of the Video::Capture::V4l
module which was created for videodev.h.

=head2 EXPORT

All functions and the variables section.

=head1 VARIABLES

=over 4

=item %codecIndexes

  This contains the index names and their position in the
  ivtv_ioctl_codec structure as returned by getCodecInfo()
  and as expected by setCodecInfo().
  
=item %capIndexes

  This contains the index names and their position in the
  v4l2_capability structure as returned by getCapabilities().

=back

=head1 FUNCTIONS

=over 4

=item @(driver, card, bus_info, version, capabilities) getCapabilities(fd)

  Returns the contents of the v4l2_capability structure.
  If the ioctl fails, then an empty list is returned.

=item @(width,height) getResolution(fd)

  Pass in the file handle number using fileno($fd) and the
  width and height are returned in an array ref.

  If an error is encountered in the ioctl call, then width
  and height will = -1.

=item int setResolution(fd, width, height)

  Specify the new width, height to set the capture to.
	Returns undef if invalid resolution is specified.
  Returns a 1 if sucess, 0 if error from the actual code.

=item int getStandard(fd)

  Returns the current video standard the card is using.

=item int setStandard(fd, standard)

  Sets the video standard to what is specified (hex value).
  Returns 1 on success, 0 on error.

=item @(index, id, name, frameperiod_numerator, frameperiod_denominator, framelines) enumerateStandard(fd, index)

  Returns the contents of the v4l2_standard structure for
  the specified index.  If the ioctl fails then index = -1.

=item int getFrequency(fd, tuner)

  Returns the current frequency for the specified tuner
  or -1 if an ioctl error occured.  Returns undef on invalid
  parameters.

=item int setFrequency(fd, tuner, freq)

  Sets the specified frequency on the specified tuner.
  Returns 1 on success, 0 on error.

=item int getInput(fd)

  Returns the current input number starting from 0.
  Returns -1 if an error occured.

=item int setInput(fd, input)

  Sets the input to the specified value.  input is a 0
  indexed value.  Returns 1 on success, 0 on error.

=item @(index, name, type, audioset, tuner, std, status) enumerateInput(fd, index)

  Returns the contents of the v4l2_input structure for
  the specified index.  If the ioctl fails then index = -1.

=item @(aspect, audio_bitmask, bframes, bitrate, bitrate_peak, dnr_mode, dnr_spatial, dnr_temporal, dnr_type, framerate, framespergop, gop_closure, pulldown, stream_type) getCodecInfo(fd)

  Returns the contents of the ivtv_ioctl_codec structure.
  If the ioctl fails then an empty list is returned.

  audio_bitmask is what audio used to be.

=item int setCodecInfo(fd, aspect, audio_bitmask, bframes, bitrate, bitrate_peak, dnr_mode, dnr_spatial, dnr_temporal, dnr_type, framerate, framespergop, gop_closure, pulldown, stream_type)

  Sets the card to the specified codec related stuff.
  Returns 1 on success, 0 on error.

  audio_bitmask is what audio used to be.

=item int setEndGOP(fd, end_gop)

  Sets the GOP_END flag to the specified value (1 or 0)
	to instruct the driver to finish a recording with a 
	closed GOP.

	Returns undef if end_gop < 0 or > 1.
	Returns 1 on success, 0 on error.

=item int stopEncoding(fd)

  Calls the VIDIOC_STREAMOFF ioctl to signal we want
	to stop encoding (reading from the driver).

	Returns 1 on success, 0 on error.

=back

=head1 AUTHOR

James A. Pattie <james at pcxperience dot com>

=head1 SEE ALSO

L<perl>.

=cut
